﻿using AutoMapper;
using SimpleAuth.Core.Domain.Entities;
using SimpleAuth.Presentation.WebApi.ViewModels;

namespace SimpleAuth.Presentation.WebApi.Mapping
{
    public class ViewModelMappingProfile : Profile
    {
        public ViewModelMappingProfile()
        {
            CreateMap<RegistrationViewModel, User>()
                .ForMember(user => user.PasswordHash, config => config.Ignore());
        }
    }
}