using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using SimpleAuth.Presentation.WebApi;

CreateHostBuilder(args).Build().Run();

IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });