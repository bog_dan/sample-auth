﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SimpleAuth.Core.Domain.Entities;
using SimpleAuth.Presentation.WebApi.Configuration;

namespace SimpleAuth.Presentation.WebApi.Services
{
    public interface IJwtGenerator
    {
        string GenerateToken(User user);
    }

    internal class JwtGenerator : IJwtGenerator
    {
        private readonly JwtConfig _jwtConfig;

        public JwtGenerator(IOptions<JwtConfig> jwtConfigOptions) => _jwtConfig = jwtConfigOptions.Value;

        public string GenerateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, user.FullName),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                }),
                Issuer = _jwtConfig.Issuer,
                Audience = _jwtConfig.Audience,
                Expires = DateTime.Now.AddMinutes(_jwtConfig.ExpirationTimeMins),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}