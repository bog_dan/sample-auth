using System;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using SimpleAuth.Common;
using SimpleAuth.Core.Application;
using SimpleAuth.Infrastructure.Mailing;
using SimpleAuth.Infrastructure.Persistent;
using SimpleAuth.Presentation.WebApi.Configuration;
using SimpleAuth.Presentation.WebApi.Services;

namespace SimpleAuth.Presentation.WebApi
{
    public class Startup
    {
        private readonly Assembly[] _assembliesToMap = new[]
        {
            typeof(Startup).Assembly,
            typeof(Infrastructure.Mailing.FrameworkExtensions).Assembly,
            typeof(Infrastructure.Persistent.FrameworkExtensions).Assembly,
        };

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddPersistentStorage(Configuration)
                .AddApplicationLayer()
                .AddMailing(Configuration)
                .AddAutoMapper(_assembliesToMap)
                .AddTransient<IJwtGenerator, JwtGenerator>()
                .Configure<JwtConfig>(Configuration.GetSection(JwtConfig.Name))
                .PostConfigure<JwtConfig>(config =>
                {
                    Require.NotNullOrEmpty(config.Audience, nameof(config.Audience));
                    Require.NotNullOrEmpty(config.Issuer, nameof(config.Issuer));
                    Require.NotNullOrEmpty(config.Secret, nameof(config.Secret));
                });
            var jwtConfig = Configuration.GetSection(JwtConfig.Name).Get<JwtConfig>();
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtConfig.Secret)),
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidIssuer = jwtConfig.Issuer,
                        ValidAudience = jwtConfig.Audience,
                        ClockSkew = TimeSpan.Zero,
                    };
                });
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "SimpleAuth.Presentation.WebApi", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(
                    c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SimpleAuth.Presentation.WebApi v1"));
            }

            app.UseCors(builder =>
                builder.WithOrigins("http://localhost:4200")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials());
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}