﻿using System.ComponentModel.DataAnnotations;

namespace SimpleAuth.Presentation.WebApi.ViewModels
{
    public class RegistrationViewModel
    {
        [Required]
        [MinLength(3)]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string FullName { get; set; }
        
        [Required]
        [MinLength(6, ErrorMessage = "Password should have at least 6 characters")]
        public string Password { get; set; }
    }
}