﻿using System.ComponentModel.DataAnnotations;

namespace SimpleAuth.Presentation.WebApi.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [MinLength(6)]
        public string Password { get; set; }
    }
}