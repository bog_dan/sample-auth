﻿namespace SimpleAuth.Presentation.WebApi.Configuration
{
    public class JwtConfig
    {
        public const string Name = "JwtConfig";
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Secret { get; set; }
        
        public int ExpirationTimeMins { get; set; }
    }
}