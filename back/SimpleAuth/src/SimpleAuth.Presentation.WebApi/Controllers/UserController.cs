﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleAuth.Core.Domain.Services;

namespace SimpleAuth.Presentation.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService) => _userService = userService;

        [Authorize]
        [HttpGet]
        public Task<string> GetName()
        {
            var userEmail = User.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;
            return _userService.GetNameByEmailAsync(userEmail);
        }
    }
}