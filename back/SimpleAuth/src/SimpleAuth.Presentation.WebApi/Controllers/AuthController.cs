﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SimpleAuth.Core.Domain.Entities;
using SimpleAuth.Core.Domain.Exceptions;
using SimpleAuth.Core.Domain.Services;
using SimpleAuth.Presentation.WebApi.Services;
using SimpleAuth.Presentation.WebApi.ViewModels;

namespace SimpleAuth.Presentation.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IJwtGenerator _jwtGenerator;

        public AuthController(IUserService userService, IMapper mapper, IJwtGenerator jwtGenerator)
        {
            _userService = userService;
            _mapper = mapper;
            _jwtGenerator = jwtGenerator;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody]
            RegistrationViewModel viewModel)
        {
            var user = _mapper.Map<User>(viewModel);
            try
            {
                await _userService.RegisterAsync(user, viewModel.Password);
            }
            catch (UserExistsException e)
            {
                var errors = new ModelStateDictionary();
                errors.AddModelError(e.ParamName ?? "", e.Message);
                return BadRequest(errors);
            }

            return NoContent();
        }

        [HttpPost("login")]
        public async Task<ActionResult<string>> LoginAsync([FromBody]
            LoginViewModel viewModel)
        {
            var user = await _userService.LoginAsync(viewModel.Username, viewModel.Password);
            if (user is not null)
            {
                var jwt = _jwtGenerator.GenerateToken(user);
                return Ok(jwt);
            }

            return BadRequest("Invalid username or password");
        }
    }
}