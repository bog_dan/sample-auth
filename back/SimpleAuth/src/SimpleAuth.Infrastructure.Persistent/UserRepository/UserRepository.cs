﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;
using Microsoft.Extensions.Options;
using SimpleAuth.Core.Domain.Repositories;
using SimpleAuth.Infrastructure.Persistent.Configurations;
using SimpleAuth.Infrastructure.Persistent.Documents;
using User = SimpleAuth.Core.Domain.Entities.User;

namespace SimpleAuth.Infrastructure.Persistent.UserRepository
{
    internal class UserRepository : IUserRepository
    {
        private readonly IMapper _mapper;
        private readonly Container _container;

        public UserRepository(IOptions<CosmosConfiguration> configuration, IMapper mapper)
        {
            _mapper = mapper;
            var client = new CosmosClient(configuration.Value.ConnectionString);
            _container = client.GetContainer(configuration.Value.DatabaseName, configuration.Value.ContainerName);
        }

        public async Task<User> GetByUsernameAsync(string username)
        {
            using var iterator = _container.GetItemLinqQueryable<UserDocument>()
                .Where(u => u.Username == username)
                .ToFeedIterator();

            if (!iterator.HasMoreResults) return null;

            var feedResponse = await iterator.ReadNextAsync();
            var userDocument = feedResponse.FirstOrDefault();
            return _mapper.Map<User>(userDocument);
        }

        public async Task<User> InsertAsync(User user)
        {
            if (user.Id == Guid.Empty)
            {
                user.Id = Guid.NewGuid();
            }

            var userDocument = _mapper.Map<UserDocument>(user);
            var response = await _container.UpsertItemAsync(userDocument, new PartitionKey(userDocument.Id));
            return _mapper.Map<User>(response.Resource);
        }

        public async Task<User> GetByEmailAsync(string email)
        {
            using var iterator = _container.GetItemLinqQueryable<UserDocument>()
                .Where(u => u.Email == email)
                .ToFeedIterator();

            if (!iterator.HasMoreResults) return null;

            var feedResponse = await iterator.ReadNextAsync();
            var userDocument = feedResponse.FirstOrDefault();
            return _mapper.Map<User>(userDocument);
        }
    }
}