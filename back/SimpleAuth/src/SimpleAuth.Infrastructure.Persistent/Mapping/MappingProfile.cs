﻿using AutoMapper;
using SimpleAuth.Core.Domain.Entities;
using SimpleAuth.Infrastructure.Persistent.Documents;

namespace SimpleAuth.Infrastructure.Persistent.Mapping
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDocument>()
                .ReverseMap();
        }
    }
}