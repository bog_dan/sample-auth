﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SimpleAuth.Common;
using SimpleAuth.Core.Domain.Repositories;
using SimpleAuth.Infrastructure.Persistent.Configurations;

namespace SimpleAuth.Infrastructure.Persistent
{
    public static class FrameworkExtensions
    {
        public static IServiceCollection AddPersistentStorage(this IServiceCollection services,
            IConfiguration configuration) =>
            services
                .Configure<CosmosConfiguration>(configuration.GetSection(CosmosConfiguration.Name))
                .PostConfigure<CosmosConfiguration>(config =>
                {
                    Require.NotNullOrEmpty(config.ConnectionString, nameof(config.ConnectionString));
                    Require.NotNullOrEmpty(config.ContainerName, nameof(config.ContainerName));
                    Require.NotNullOrEmpty(config.DatabaseName, nameof(config.DatabaseName));
                })
                .AddTransient<IUserRepository, UserRepository.UserRepository>();
    }
}