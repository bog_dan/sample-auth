﻿namespace SimpleAuth.Infrastructure.Persistent.Configurations
{
    internal class CosmosConfiguration
    {
        public const string Name = "CosmosDb";
        public string DatabaseName { get; set; }
        public string ContainerName { get; set; }
        public string ConnectionString { get; set; }
    }
}