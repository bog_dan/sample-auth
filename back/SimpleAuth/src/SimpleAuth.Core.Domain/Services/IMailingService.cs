﻿using System.Threading.Tasks;

namespace SimpleAuth.Core.Domain.Services
{
    public interface IMailingService
    {
        Task SendGreetingMessage(string email);
    }
}