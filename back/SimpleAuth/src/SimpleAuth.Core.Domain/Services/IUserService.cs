﻿using System.Threading.Tasks;
using SimpleAuth.Core.Domain.Entities;

namespace SimpleAuth.Core.Domain.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Registers user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns>Registered user</returns>
        Task<User> RegisterAsync(User user, string password);

        /// <summary>
        /// Tries to log in user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns><code>null</code> if invalid username or password, else returns user</returns>
        Task<User> LoginAsync(string username, string password);
        /// <summary>
        /// Gets fullname by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        Task<string> GetNameByEmailAsync(string email);
    }
}