﻿using System.Threading.Tasks;
using SimpleAuth.Core.Domain.Entities;

namespace SimpleAuth.Core.Domain.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetByUsernameAsync(string username);
        Task<User> InsertAsync(User user);
        
        Task<User> GetByEmailAsync(string email);
    }
}