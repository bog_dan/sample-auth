﻿using System;
using System.Runtime.Serialization;

namespace SimpleAuth.Core.Domain.Exceptions
{
    [Serializable]
    public class UserExistsException : ArgumentException
    {
        public UserExistsException()
        {
        }

        public UserExistsException(string paramName) : base($"User with this {paramName} already exists", paramName)
        {
        }

        public UserExistsException(string message, Exception inner) : base(message, inner)
        {
        }

        protected UserExistsException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}