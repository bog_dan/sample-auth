﻿using System;

namespace SimpleAuth.Common
{
    public static class Require
    {
        public static void NotNull(object value, string paramName)
        {
            if (value is null)
            {
                throw new ArgumentException("Value cannot be null", paramName);
            }
        }

        public static void NotNullOrEmpty(string value, string paramName)
        {
            if (value is null)
            {
                throw new ArgumentException("Expected non-empty string", paramName);
            }
        }
    }
}