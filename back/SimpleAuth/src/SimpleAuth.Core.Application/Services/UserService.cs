﻿using System.Threading.Tasks;
using SimpleAuth.Core.Domain.Entities;
using SimpleAuth.Core.Domain.Exceptions;
using SimpleAuth.Core.Domain.Repositories;
using SimpleAuth.Core.Domain.Services;

namespace SimpleAuth.Core.Application.Services
{
    using BCrypt = BCrypt.Net.BCrypt;

    internal class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMailingService _mailingService;

        public UserService(IUserRepository userRepository, IMailingService mailingService)
        {
            _userRepository = userRepository;
            _mailingService = mailingService;
        }

        public async Task<User> RegisterAsync(User user, string password)
        {
            var byEmail = await _userRepository.GetByEmailAsync(user.Email);
            if (byEmail is not null)
            {
                throw new UserExistsException(nameof(user.Email));
            }

            var byUsername = await _userRepository.GetByUsernameAsync(user.Username);
            if (byUsername is not null)
            {
                throw new UserExistsException(nameof(user.Username));
            }

            var hashedPassword = BCrypt.HashPassword(password);
            user.PasswordHash = hashedPassword;
            var inserted = await _userRepository.InsertAsync(user);
            await _mailingService.SendGreetingMessage(user.Email);
            return inserted;
        }

        public async Task<User> LoginAsync(string username, string password)
        {
            var user = await _userRepository.GetByUsernameAsync(username);
            if (user is null) return null;
            var isValid = BCrypt.Verify(password, user.PasswordHash);
            return isValid ? user : null;
        }

        public async Task<string> GetNameByEmailAsync(string userEmail)
        {
            var user = await _userRepository.GetByEmailAsync(userEmail);
            return user.FullName;
        }
    }
}