﻿using Microsoft.Extensions.DependencyInjection;
using SimpleAuth.Core.Application.Services;
using SimpleAuth.Core.Domain.Services;

namespace SimpleAuth.Core.Application
{
    public static class FrameworkExtensions
    {
        public static IServiceCollection AddApplicationLayer(this IServiceCollection services) =>
            services.AddTransient<IUserService, UserService>();
    }
}