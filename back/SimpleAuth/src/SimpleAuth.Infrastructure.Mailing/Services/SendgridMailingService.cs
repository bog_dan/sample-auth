﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using SimpleAuth.Core.Domain.Services;
using SimpleAuth.Infrastructure.Mailing.Configuration;

namespace SimpleAuth.Infrastructure.Mailing.Services
{
    internal class SendgridMailingService : IMailingService
    {
        private readonly SendgridConfiguration _configuration;

        public SendgridMailingService(IOptions<SendgridConfiguration> configuration) =>
            _configuration = configuration.Value;

        public async Task SendGreetingMessage(string email)
        {
            var client = new SendGridClient(_configuration.ApiKey);
            var from = new EmailAddress(_configuration.SenderEmail, _configuration.SenderName);
            var subject = "Greetings";
            var to = new EmailAddress(email);
            var plainTextContent = "Welcome!";
            var htmlContent = "<strong>You have created an account</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            await client.SendEmailAsync(msg);
        }
    }
}