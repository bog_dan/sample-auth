﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SimpleAuth.Common;
using SimpleAuth.Core.Domain.Services;
using SimpleAuth.Infrastructure.Mailing.Configuration;
using SimpleAuth.Infrastructure.Mailing.Services;

namespace SimpleAuth.Infrastructure.Mailing
{
    public static class FrameworkExtensions
    {
        public static IServiceCollection AddMailing(this IServiceCollection services, IConfiguration configuration) =>
            services.Configure<SendgridConfiguration>(configuration.GetSection(SendgridConfiguration.Name))
                .PostConfigure<SendgridConfiguration>(sendgridConfiguration =>
                {
                    Require.NotNullOrEmpty(sendgridConfiguration.ApiKey, nameof(sendgridConfiguration.ApiKey));
                    Require.NotNullOrEmpty(sendgridConfiguration.SenderEmail,
                        nameof(sendgridConfiguration.SenderEmail));
                    Require.NotNullOrEmpty(sendgridConfiguration.SenderName, nameof(sendgridConfiguration.SenderName));
                })
                .AddTransient<IMailingService, SendgridMailingService>();
    }
}