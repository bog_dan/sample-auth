﻿namespace SimpleAuth.Infrastructure.Mailing.Configuration
{
    internal class SendgridConfiguration
    {
        public const string Name = "Sendgrid";
        public string ApiKey { get; set; }
        public string SenderEmail { get; set; }
        public string SenderName { get; set; }
    }
}