﻿using System.Threading.Tasks;
using Moq;
using SimpleAuth.Core.Application.Services;
using SimpleAuth.Core.Application.Tests.EqualityComparers;
using SimpleAuth.Core.Domain.Entities;
using SimpleAuth.Core.Domain.Exceptions;
using SimpleAuth.Core.Domain.Services;
using Xunit;

namespace SimpleAuth.Core.Application.Tests
{
    public class AuthorizationTests
    {
        [Fact]
        public async Task Login_AfterSuccessfulRegistration_ShouldWork()
        {
            var mailingServiceMock = new Mock<IMailingService>();
            var userService = new UserService(new FakeUserRepository(), mailingServiceMock.Object);
            var user = new User()
            {
                Email = "test@test.com",
                FullName = "Test User",
                Username = "Username"
            };
            string password = "password123";

            user = await userService.RegisterAsync(user, password);
            mailingServiceMock.Verify(service => service.SendGreetingMessage(user.Email), Times.Once);

            var loggedInUser = await userService.LoginAsync(user.Username, password);

            Assert.Equal(user, loggedInUser, new UserEqualityComparer());
        }

        [Fact]
        public async Task Registration_WithSameEmails_ShouldThrowUserExistsException()
        {
            var mailingServiceMock = new Mock<IMailingService>();
            var userService = new UserService(new FakeUserRepository(), mailingServiceMock.Object);
            string password = "asdas";
            var user1 = new User()
            {
                Email = "test@test.com",
                FullName = "Test user",
                Username = "Username"
            };
            var user2 = new User()
            {
                Email = "test@test.com",
                FullName = "Test user",
                Username = "another username"
            };

            await userService.RegisterAsync(user1, password);
            await Assert.ThrowsAsync<UserExistsException>(nameof(user1.Email),
                async () => await userService.RegisterAsync(user2, password));
            mailingServiceMock.Verify(service => service.SendGreetingMessage(user1.Email), Times.Once);
        }

        [Fact]
        public async Task Registration_WithSameUsername_ShouldThrowUserExistsException()
        {
            var mailingServiceMock = new Mock<IMailingService>();
            var userService = new UserService(new FakeUserRepository(), mailingServiceMock.Object);
            string password = "asdas";
            var user1 = new User()
            {
                Email = "test@test.com",
                FullName = "Test user",
                Username = "Username"
            };
            var user2 = new User()
            {
                Email = "anothertest@test.com",
                FullName = "Test user",
                Username = "Username"
            };

            await userService.RegisterAsync(user1, password);
            await Assert.ThrowsAsync<UserExistsException>(nameof(user2.Username),
                async () => await userService.RegisterAsync(user2, password));
        }
    }
}