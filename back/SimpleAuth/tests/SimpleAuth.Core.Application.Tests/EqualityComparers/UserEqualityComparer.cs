﻿using System;
using System.Collections.Generic;
using SimpleAuth.Core.Domain.Entities;

namespace SimpleAuth.Core.Application.Tests.EqualityComparers
{
    public class UserEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals(User x, User y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Id.Equals(y.Id) && x.Username == y.Username && x.Email == y.Email && x.FullName == y.FullName && x.PasswordHash == y.PasswordHash;
        }

        public int GetHashCode(User obj)
        {
            return HashCode.Combine(obj.Id, obj.Username, obj.Email, obj.FullName, obj.PasswordHash);
        }
    }
}