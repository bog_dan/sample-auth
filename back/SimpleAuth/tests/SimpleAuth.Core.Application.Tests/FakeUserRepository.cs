﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleAuth.Core.Domain.Entities;
using SimpleAuth.Core.Domain.Repositories;

namespace SimpleAuth.Core.Application.Tests
{
    public class FakeUserRepository : IUserRepository
    {
        private readonly List<User> _users = new();

        public Task<User> GetByUsernameAsync(string username)
        {
            var user = _users.FirstOrDefault(u => u.Username == username);
            return Task.FromResult(user);
        }

        public Task<User> InsertAsync(User user)
        {
            _users.Add(user);
            return Task.FromResult(user);
        }

        public Task<User> GetByEmailAsync(string email)
        {
            var user = _users.FirstOrDefault(u => u.Email == email);
            return Task.FromResult(user);
        }
    }
}