﻿# Backend

## 1. Description

An application is implemented using .NET 5, CosmosDB and Sendgrid

## 2. Start application

1. Set missing fields in "appsettings.Development.json", such as CosmosDB connection string and Sendgrid configuration

*no need to check git log, I haven't accidentally included creds in any commit:)*


2. `dotnet run --project  src\SimpleAuth.Presentation.WebApi`