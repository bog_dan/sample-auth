import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthClient, RegistrationViewModel } from '../services/auth.client';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnDestroy {
  private readonly ngOnDestroySubj = new Subject();

  public signupForm: FormGroup;
  public error: string | null = null;
  public loading = false;
  public hide = true;

  constructor(private readonly authClient: AuthClient, private readonly router: Router) {
    this.signupForm = new FormGroup({
      fullName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(25), Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@!#$%^&+=]).*$')]),
      username: new FormControl('', [Validators.required])
    });
  }

  ngOnDestroy(): void {
    this.ngOnDestroySubj.next();
    this.ngOnDestroySubj.complete();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.signupForm.controls[controlName].hasError(errorName);
  };

  public signup = (signupFormValue: any) => {

    if (this.signupForm.valid) {

      let accountSignup = {
        fullName: signupFormValue.fullName,
        email: signupFormValue.email,
        password: signupFormValue.password,
        username: signupFormValue.username
      };

      this.authClient.register(new RegistrationViewModel(accountSignup))
        .pipe(takeUntil(this.ngOnDestroySubj))
        .subscribe(() => this.router.navigate([''], {replaceUrl: true}),
          (e) => {
            if (e.status === 400) {
              const response = JSON.parse(e.response);
              if (Reflect.has(response, 'Email')) {
                this.signupForm.get('email')?.setErrors({taken: true});
              } else if (Reflect.has(response, 'Username')) {
                this.signupForm.get('username')?.setErrors({taken: true});
              }
            }
          });

    }
  };
}
