import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthClient, LoginViewModel } from '../services/auth.client';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  public signinForm: FormGroup;
  public error: string | null = null;
  public loading = false;
  public hide = true;

  constructor(private readonly authClient: AuthClient,
              private readonly router: Router,
              private readonly authService: AuthService) {
    this.signinForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(25)])
    });
  }

  ngOnInit() {

  }

  public hasError = (controlName: string, errorName: string) => {
    return this.signinForm.controls[controlName].hasError(errorName);
  };

  public signup = (signupFormValue: any) => {

    if (this.signinForm.valid) {

      let accountSignup = {
        password: signupFormValue.password,
        username: signupFormValue.username
      };
      this.authClient.login(new LoginViewModel(accountSignup))
        .pipe(catchError(() => {
          this.error = 'Incorrect username or password';
          this.signinForm.controls['username'].hasError('invalid');
          this.signinForm.controls['password'].hasError('invalid');
          return of(null);
        }))
        .subscribe((token) => {
          if (token) {
            this.authService.token = token;
            this.router.navigate(['profile'], {replaceUrl: true});
          }
        });
    }
  };

}
