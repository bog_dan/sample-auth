import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class AppConfig {
  public get apiUrl() {
    return environment.apiUrl;
  }
}
