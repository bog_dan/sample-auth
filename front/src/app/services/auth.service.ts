import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class AuthService {
  private readonly accessTokenKey = 'accessToken';

  public set token(token: string | null) {
    if (token) {
      // normally cookie should be used
      localStorage.setItem(this.accessTokenKey, token);
    }
  };

  public get token(): string | null {
    return localStorage.getItem(this.accessTokenKey);
  }

  constructor() {
  }
}
