import { Component, OnDestroy } from '@angular/core';
import { UserClient } from '../services/user.client';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnDestroy {
  private readonly ngOnDestroySubj = new Subject();

  constructor(private readonly userClient: UserClient) {
  }

  public showName() {
    this.userClient.getName()
      .pipe(takeUntil(this.ngOnDestroySubj))
      .subscribe((name) => alert(name),
        (e) => {
          if (e.status === 401) {
            alert('You are not authorized');
          }
        });
  }

  ngOnDestroy(): void {
    this.ngOnDestroySubj.next();
    this.ngOnDestroySubj.complete();
  }

}
